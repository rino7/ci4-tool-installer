<?php

namespace Dolmedo\CodeIgniter4ToolInstaller;

use Composer\Installers\Installer as ComposerInstaller;
use Composer\Package\PackageInterface;

class CodeIgniter4ToolInstaller extends ComposerInstaller
{

    protected $packageTypes;

    public function getInstallPath(PackageInterface $package)
    {
        return '.';
    }

    public function supports($packageType)
    {
        return 'diegodev-ci4tool' === $packageType;
    }

}
