<?php

namespace Dolmedo\CodeIgniter4ToolInstaller;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

class CodeIgniter4ToolPlugin implements PluginInterface
{

    public function activate(Composer $composer, IOInterface $io)
    {
        $installer = new CodeIgniter4ToolInstaller($io, $composer);
        $composer->getInstallationManager()->addInstaller($installer);
    }

}
